package ru.iteco.behavioral.chain.bancomat.banknote;

/**
 * BanknoteHandler.
 *
 * @author Ilya_Sukhachev
 */
public abstract class BanknoteHandler {

    protected BanknoteHandler nextHandler;

    protected BanknoteHandler(BanknoteHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public boolean validate(String banknote) {
        return nextHandler != null && nextHandler.validate(banknote);
    }

    public String doWithdraw(String value) {
        Integer intValue = Integer.valueOf(value);
        while (intValue >= getValue()) {
            intValue = intValue - getValue();
        }
        if (nextHandler != null) {
            return nextHandler.doWithdraw(String.valueOf(intValue));
        }
        return value;
    }

    protected abstract int getValue();
}
