package ru.iteco.behavioral.chain.bancomat;

import ru.iteco.behavioral.chain.bancomat.banknote.Banknote;
import ru.iteco.behavioral.chain.bancomat.banknote.CurrencyType;
import ru.iteco.behavioral.chain.bancomat.dollar.DollarHandlerBase;
import ru.iteco.behavioral.chain.bancomat.dollar.FiftyDollarHandler;
import ru.iteco.behavioral.chain.bancomat.dollar.HundredDollarHandler;
import ru.iteco.behavioral.chain.bancomat.dollar.TenDollarHandler;
import ru.iteco.behavioral.chain.bancomat.euro.EuroHandlerBase;
import ru.iteco.behavioral.chain.bancomat.euro.FiftyEuroHandler;
import ru.iteco.behavioral.chain.bancomat.euro.FiveEuroHandler;
import ru.iteco.behavioral.chain.bancomat.euro.TenEuroHandler;
import ru.iteco.behavioral.chain.bancomat.ruble.FiftyRubleHandler;
import ru.iteco.behavioral.chain.bancomat.ruble.HundredRubleHandler;
import ru.iteco.behavioral.chain.bancomat.ruble.RubleHandlerBase;
import ru.iteco.behavioral.chain.bancomat.ruble.TenRubleHandler;

/**
 * Bancomat.
 *
 * @author Ilya_Sukhachev
 */
public class Bancomat {
    private RubleHandlerBase rubleHandler;

    private DollarHandlerBase dollarHandler;

    private EuroHandlerBase euroHandler;

    public Bancomat() {
        rubleHandler = new HundredRubleHandler(null);
        rubleHandler = new FiftyRubleHandler(rubleHandler);
        rubleHandler = new TenRubleHandler(rubleHandler);

        dollarHandler = new HundredDollarHandler(null);
        dollarHandler = new FiftyDollarHandler(dollarHandler);
        dollarHandler = new TenDollarHandler(dollarHandler);

        euroHandler = new FiftyEuroHandler(null);
        euroHandler = new TenEuroHandler(euroHandler);
        euroHandler = new FiveEuroHandler(euroHandler);
    }

    public boolean validate(String banknote) {
        return rubleHandler.validate(banknote) ||
                dollarHandler.validate(banknote) ||
                euroHandler.validate(banknote);
    }

    public void doWithdraw(Banknote banknote) {
        final String change;
        String value = banknote.getValue();
        switch (banknote.getCurrency()) {
            case EUR:
                change = euroHandler.doWithdraw(banknote.getValue());
                break;
            case RUB:
                change = rubleHandler.doWithdraw(banknote.getValue());
                break;
            case USD:
                change = dollarHandler.doWithdraw(banknote.getValue());
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + banknote.getCurrency());
        }
        String currency = banknote.getCurrency().name();
        if (change.equals(banknote.getValue())) {
            System.out.printf("Не получилось обналичить: %s %s", value, currency);
            System.out.println();
        } else {
            int cash = Integer.valueOf(value) - Integer.valueOf(change);
            System.out.printf("Обналичено: %s %s, остаток: %s %s", cash, currency, change, currency);
            System.out.println();
        }
    }

}
