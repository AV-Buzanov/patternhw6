package ru.iteco.behavioral.chain.bancomat.ruble;

import ru.iteco.behavioral.chain.bancomat.banknote.BanknoteHandler;

/**
 * TenRubleHandler.
 *
 * @author Ilya_Sukhachev
 */
public class FiftyRubleHandler extends RubleHandlerBase {
    public FiftyRubleHandler(RubleHandlerBase nextHandler) {
        super(nextHandler);
    }
    protected int value = 50;

    @Override
    public boolean validate(String banknote) {
        if (banknote.equals("50 Рублей")) {
            return true;
        }
        return super.validate(banknote);
    }

    @Override
    protected int getValue() {
        return value;
    }
}
