package ru.iteco.behavioral.chain.bancomat.ruble;

import ru.iteco.behavioral.chain.bancomat.banknote.BanknoteHandler;

/**
 * TenRubleHandler.
 *
 * @author Ilya_Sukhachev
 */
public class HundredRubleHandler extends RubleHandlerBase {
    public HundredRubleHandler(RubleHandlerBase nextHandler) {
        super(nextHandler);
    }
    protected int value = 100;

    @Override
    public boolean validate(String banknote) {
        if (banknote.equals("100 Рублей")) {
            return true;
        }
        return super.validate(banknote);
    }

    @Override
    protected int getValue() {
        return value;
    }
}
