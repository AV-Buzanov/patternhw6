package ru.iteco.behavioral.chain.bancomat;

import ru.iteco.behavioral.chain.bancomat.banknote.Banknote;
import ru.iteco.behavioral.chain.bancomat.banknote.CurrencyType;

/**
 * Main.
 *
 * @author Ilya_Sukhachev
 */
public class Main {

    public static void main(String[] args) {
        Bancomat bancomat = new Bancomat();
        System.out.println(bancomat.validate("100"));

        bancomat.doWithdraw(new Banknote(CurrencyType.RUB, "111"));
        bancomat.doWithdraw(new Banknote(CurrencyType.USD, "8"));
        bancomat.doWithdraw(new Banknote(CurrencyType.EUR, "118"));

    }
}
